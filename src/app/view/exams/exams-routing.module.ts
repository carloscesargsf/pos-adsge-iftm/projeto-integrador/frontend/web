import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListExamsComponent } from './list-exams/list-exams.component';
import { FormExamComponent } from './form-exam/form-exam.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Exams'
    },
    children: [
      {
        path: '',
        component: ListExamsComponent,
        data: {
          title: 'List Exams',
        },
      },
      {
        path: 'new',
        component: FormExamComponent,
        data: {
          title: 'New Exam',
        },
      },
      {
        path: ':id',
        component: FormExamComponent,
        data: {
          title: 'Edit Exam',
        },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExamsRoutingModule { }
