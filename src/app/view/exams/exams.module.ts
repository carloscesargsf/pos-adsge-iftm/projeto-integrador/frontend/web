import { NgModule } from '@angular/core';
import { ExamsRoutingModule } from './exams-routing.module';
import { ListExamsComponent } from './list-exams/list-exams.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormExamComponent } from './form-exam/form-exam.component';
import { TabsModule } from 'ngx-bootstrap/tabs';

@NgModule({
  imports: [
    ExamsRoutingModule,
    SharedModule,
    TabsModule
  ],
  declarations: [
    ListExamsComponent,
    FormExamComponent,
  ]
})
export class ExamsModule { }
