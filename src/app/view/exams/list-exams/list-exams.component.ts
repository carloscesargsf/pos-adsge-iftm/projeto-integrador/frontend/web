import { Component, OnInit } from '@angular/core';
import { ExamsService } from 'src/app/core/services/exams/exams.service';
import { PageInfo } from 'src/app/core/models/page-info';
import { ExamViewItem } from 'src/app/core/models/exam-view-item.model';

@Component({
  selector: 'app-list-exams',
  templateUrl: './list-exams.component.html',
  styleUrls: ['./list-exams.component.scss']
})
export class ListExamsComponent implements OnInit {

  payLoad: PageInfo<ExamViewItem> = new PageInfo<ExamViewItem>();

  constructor(private examsService: ExamsService) {
  }

  ngOnInit() {
    this.fetchData();
  }

  fetchData() {
    this.examsService.findAll()
      .subscribe(payLoad => this.payLoad = payLoad);
  }

  delete(id: number) {
    this.examsService.delete(id).subscribe(
      () => this.fetchData(),
      error => console.warn(error),
    );
  }

}
