import { Component, OnInit } from '@angular/core';
import { ExamsService } from 'src/app/core/services/exams/exams.service';
import { ExamInsert } from 'src/app/core/models/exam-insert.model';
import { ExamUpdate } from 'src/app/core/models/exam-update.model';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormArray, FormControl } from '@angular/forms';
import { QuestionViewItem } from 'src/app/core/models/question-view-item.model';
import { PageInfo } from 'src/app/core/models/page-info';
import { QuestionsService } from 'src/app/core/services/questions/questions.service';
import { CategoriesService } from 'src/app/core/services/categories/categories.service';
import { CategoryViewItem } from 'src/app/core/models/category-view-item.model';
import { CategoryList } from 'src/app/core/models/category-list.model';
import { PersonViewItem } from 'src/app/core/models/person-view-item.model';
import { PeopleService } from 'src/app/core/services/people/people.service';
import { PeopleRolesService } from 'src/app/core/services/people-roles/people-roles.service';
import { PersonRoleViewItem } from 'src/app/core/models/person-role-view-item.model';

@Component({
  selector: 'app-form-exam',
  templateUrl: './form-exam.component.html',
  styleUrls: ['./form-exam.component.scss']
})
export class FormExamComponent implements OnInit {

  id: number;
  exam: FormGroup;
  categories: PageInfo<CategoryList> = new PageInfo<CategoryList>();
  questions: PageInfo<QuestionViewItem> = new PageInfo<QuestionViewItem>();
  rated: PageInfo<PersonRoleViewItem> = new PageInfo<PersonRoleViewItem>();
  categoryToFilter: number;

  constructor(private examsService: ExamsService,
              private categoriesService: CategoriesService,
              private questionsService: QuestionsService,
              private peopleRolesService: PeopleRolesService,
              private currentRoute: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    this.fetchData();
    this.fetchCategories();
    this.fetchQuestions();
    this.fetchRated();
    this.categoryToFilter = 0;
  }

  fetchData() {
    // tslint:disable-next-line: no-string-literal
    this.currentRoute.params.subscribe(
      params => {
        // tslint:disable-next-line: no-string-literal
        this.id = params['id'];
        if (this.id) {
          this.exam = ExamUpdate.getFormGroup();
          this.examsService.findById(this.id).subscribe(
            exam => {
              this.exam.patchValue(exam);
              const checkArrayQuestionsIds: FormArray = this.exam.get('questionIds') as FormArray;
              exam.questions.forEach(examQuestion => checkArrayQuestionsIds.push(new FormControl(examQuestion.question.id)));
              const checkArrayRatedIds: FormArray = this.exam.get('ratedIds') as FormArray;
              exam.rated.forEach(examRated => checkArrayRatedIds.push(new FormControl(examRated.ratedId)));
            },
            () => this.router.navigate(['/404']),
          );
        } else {
          this.exam = ExamInsert.getFormGroup();
        }
      },
    );
  }

  fetchCategories() {
    this.categoriesService.findAll()
      .subscribe(payLoad => this.categories = payLoad);
  }

  fetchQuestions() {
    let filter = '';
    if (this.categoryToFilter && this.categoryToFilter > 0) {
      filter += `categoryId=${this.categoryToFilter}`;
    }
    this.questionsService.findAll(filter)
      .subscribe(payLoad => this.questions = payLoad);
  }

  fetchRated() {
    this.peopleRolesService.findAll('role=RATED')
      .subscribe(payLoad => this.rated = payLoad);
  }

  onSubmit() {
    console.log(this.exam.value);
    if (this.exam.valid) {
      if (!this.id) {
        this.examsService.create(this.exam.value).subscribe(
          () => this.onGoBack(),
          error => console.error('create error', error),
        );
      } else {
        this.examsService.update(this.id, this.exam.value).subscribe(
          () => this.onGoBack(),
          error => console.error('update error', error),
        );
      }
    } else {
      console.error(this.exam.errors);
    }
  }

  onGoBack() {
    this.router.navigate(['/exams']);
  }

  onCheckboxChange(e, field: string) {
    const checkArray: FormArray = this.exam.get(field) as FormArray;
    const targetValue = Number(e.target.value);

    if (e.target.checked) {
      checkArray.push(new FormControl(targetValue));
    } else {
      let i = 0;
      checkArray.controls.forEach((item: FormControl) => {
        if (item.value === targetValue) {
          checkArray.removeAt(i);
          return;
        }
        i++;
      });
    }
  }

  isChecked(value, field: string) {
    const checkArray: FormArray = this.exam.get(field) as FormArray;

    return checkArray.controls.filter((item: FormControl) => item.value === value).length > 0;
  }

}
