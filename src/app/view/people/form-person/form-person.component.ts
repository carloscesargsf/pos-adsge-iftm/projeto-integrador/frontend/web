import { Component, OnInit } from '@angular/core';
import { PeopleService } from 'src/app/core/services/people/people.service';
import { PersonInsert } from 'src/app/core/models/person-insert.model';
import { PersonUpdate } from 'src/app/core/models/person-update.model';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, Validators, FormArray, FormControl } from '@angular/forms';
import { PersonRoleViewItem } from 'src/app/core/models/person-role-view-item.model';

@Component({
  selector: 'app-form-person',
  templateUrl: './form-person.component.html',
  styleUrls: ['./form-person.component.scss']
})
export class FormPersonComponent implements OnInit {

  id: number;
  person: FormGroup;

  constructor(private peopleService: PeopleService,
              private currentRoute: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    this.fetchData();
    this.setPasswordValidators();
  }

  fetchData() {
    // tslint:disable-next-line: no-string-literal
    this.currentRoute.params.subscribe(
      params => {
        // tslint:disable-next-line: no-string-literal
        this.id = params['id'];
        if (this.id) {
          this.person = PersonUpdate.getFormGroup();
          this.peopleService.findById(this.id).subscribe(
            person => {
              person.gender = person.gender === 'Female' ? 'F' : 'M';
              this.person.patchValue(person);
              const checkArray: FormArray = this.person.get('roles') as FormArray;
              person.personRoles
                .forEach((personRole: PersonRoleViewItem) => checkArray.push(new FormControl(personRole.role.toUpperCase())));
            },
            () => this.router.navigate(['/404']),
          );
        } else {
          this.person = PersonInsert.getFormGroup();
        }
      },
    );
  }

  setPasswordValidators() {
    if (this.id) {
      const passwordControl = this.person.get('password');

      this.person.get('password').valueChanges
        .subscribe((password: string) => {
          password && password.length > 0
            ? passwordControl.setValidators([Validators.required, Validators.minLength(8), Validators.maxLength(200)])
            : passwordControl.setValidators([]);
          passwordControl.updateValueAndValidity();
        });
    }
  }

  onSubmit() {
    if (this.person.valid) {
      if (!this.id) {
        this.peopleService.create(this.person.value).subscribe(
          () => this.router.navigate(['/people']),
          error => console.error('create error', error),
        );
      } else {
        this.peopleService.update(this.id, this.person.value).subscribe(
          () => this.router.navigate(['/people']),
          error => console.error('update error', error),
        );
      }
    } else {
      console.error(this.person.errors);
    }
  }

  onGoBack() {
    this.router.navigate(['/people']);
  }

  onCheckboxChange(e) {
    const checkArray: FormArray = this.person.get('roles') as FormArray;
    const targetValue = String(e.target.value);

    if (e.target.checked) {
      checkArray.push(new FormControl(targetValue));
    } else {
      let i = 0;
      checkArray.controls.forEach((item: FormControl) => {
        if (item.value === targetValue) {
          checkArray.removeAt(i);
          return;
        }
        i++;
      });
    }
  }

  isChecked(value) {
    const checkArray: FormArray = this.person.get('roles') as FormArray;

    console.log('value', value);
    return checkArray.controls.filter((item: FormControl) => item.value === value).length > 0;
  }

}
