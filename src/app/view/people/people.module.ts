import { NgModule } from '@angular/core';
import { PeopleRoutingModule } from './people-routing.module';
import { ListPeopleComponent } from './list-people/list-people.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormPersonComponent } from './form-person/form-person.component';

@NgModule({
  imports: [
    PeopleRoutingModule,
    SharedModule,
  ],
  declarations: [
    ListPeopleComponent,
    FormPersonComponent,
  ]
})
export class PeopleModule { }
