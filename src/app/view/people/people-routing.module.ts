import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListPeopleComponent } from './list-people/list-people.component';
import { FormPersonComponent } from './form-person/form-person.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'People'
    },
    children: [
      {
        path: '',
        component: ListPeopleComponent,
        data: {
          title: 'List People',
        },
      },
      {
        path: 'new',
        component: FormPersonComponent,
        data: {
          title: 'New Person',
        },
      },
      {
        path: ':id',
        component: FormPersonComponent,
        data: {
          title: 'Edit Person',
        },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PeopleRoutingModule { }
