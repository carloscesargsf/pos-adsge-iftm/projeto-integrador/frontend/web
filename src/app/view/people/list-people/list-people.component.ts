import { Component, OnInit } from '@angular/core';
import { PeopleService } from 'src/app/core/services/people/people.service';
import { PersonViewItem } from 'src/app/core/models/person-view-item.model';
import { PageInfo } from 'src/app/core/models/page-info';

@Component({
  selector: 'app-list-people',
  templateUrl: './list-people.component.html',
  styleUrls: ['./list-people.component.scss']
})
export class ListPeopleComponent implements OnInit {

  payLoad: PageInfo<PersonViewItem> = new PageInfo<PersonViewItem>();

  constructor(private peopleService: PeopleService) {
  }

  ngOnInit() {
    this.fetchData();
  }

  fetchData() {
    this.peopleService.findAll()
      .subscribe(payLoad => this.payLoad = payLoad);
  }

}
