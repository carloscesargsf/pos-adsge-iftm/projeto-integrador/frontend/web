import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';
import { CompaniesService } from 'src/app/core/services/companies/companies.service';
import { CompanyInsert } from 'src/app/core/models/company-insert.model';

@Component({
  selector: 'app-dashboard',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  company: FormGroup;
  error: string;

  constructor(private companiesService: CompaniesService,
              private router: Router) { }

  ngOnInit() {
    this.company = CompanyInsert.getFormGroup();
  }

  onSubmit() {
    console.log(this.company.value);
    if (this.company.valid) {
      this.companiesService.create(this.company.value)
        .pipe(first())
        .subscribe(
          () => this.router.navigate(['/login']),
          error => {
            this.error = error;
          }
        );
    } else {
      console.error(this.company.errors);
    }
  }

}
