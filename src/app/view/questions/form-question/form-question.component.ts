import { Component, OnInit } from '@angular/core';
import { QuestionsService } from 'src/app/core/services/questions/questions.service';
import { QuestionInsert } from 'src/app/core/models/question-insert.model';
import { QuestionUpdate } from 'src/app/core/models/question-update.model';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormArray } from '@angular/forms';
import { CategoryList } from 'src/app/core/models/category-list.model';
import { CategoriesService } from 'src/app/core/services/categories/categories.service';
import { PageInfo } from 'src/app/core/models/page-info';
import { QuestionTypesService } from 'src/app/core/services/question-types/question-types.service';
import { QuestionTypeViewItem } from 'src/app/core/models/question-type-view-item.model';
import { QuestionPossibleAnswer } from 'src/app/core/models/question-possible-answer.model';

@Component({
  selector: 'app-form-question',
  templateUrl: './form-question.component.html',
  styleUrls: ['./form-question.component.scss']
})
export class FormQuestionComponent implements OnInit {

  id: number;
  question: FormGroup;
  questionTypes: PageInfo<QuestionTypeViewItem> = new PageInfo<QuestionTypeViewItem>();
  categories: PageInfo<CategoryList> = new PageInfo<CategoryList>();
  possibleAnswers: FormArray;

  constructor(private questionsService: QuestionsService,
              private questionTypesService: QuestionTypesService,
              private categoriesService: CategoriesService,
              private currentRoute: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    this.fetchData();
    this.fetchQuestionTypes();
    this.fetchCategories();
    this.possibleAnswers = this.question.get('possibleAnswers') as FormArray;
  }

  fetchQuestionTypes() {
    this.questionTypesService.findAll()
      .subscribe(payLoad => this.questionTypes = payLoad);
  }

  fetchCategories() {
    this.categoriesService.findAll()
      .subscribe(payLoad => this.categories = payLoad);
  }

  fetchData() {
    // tslint:disable-next-line: no-string-literal
    this.currentRoute.params.subscribe(
      params => {
        // tslint:disable-next-line: no-string-literal
        this.id = params['id'];
        if (this.id) {
          this.question = QuestionUpdate.getFormGroup();
          this.questionsService.findById(this.id).subscribe(
            q => {
              this.question.patchValue(q);
              const possibleAnswers: FormArray = this.question.get('possibleAnswers') as FormArray;
              q.possibleAnswers.forEach(possibleAnswer => {
                const questionPossibleAnswer = new QuestionPossibleAnswer();
                questionPossibleAnswer.description = possibleAnswer.description;
                questionPossibleAnswer.correct = possibleAnswer.correct;
                const qpa: FormGroup = QuestionPossibleAnswer.getFormGroup();
                qpa.patchValue(questionPossibleAnswer);
                possibleAnswers.push(qpa);
              });
            },
            () => this.router.navigate(['/404']),
          );
        } else {
          this.question = QuestionInsert.getFormGroup();
        }
      },
    );
  }

  onSubmit() {
    if (this.question.valid) {
      if (!this.id) {
        this.questionsService.create(this.question.value).subscribe(
          () => this.router.navigate(['/questions']),
          error => console.error('create error', error),
        );
      } else {
        this.questionsService.update(this.id, this.question.value).subscribe(
          () => this.router.navigate(['/questions']),
          error => console.error('update error', error),
        );
      }
    } else {
      console.error(this.question.errors);
    }
  }

  onGoBack() {
    this.router.navigate(['/questions']);
  }

}
