import { NgModule } from '@angular/core';
import { QuestionsRoutingModule } from './questions-routing.module';
import { ListQuestionsComponent } from './list-questions/list-questions.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormQuestionComponent } from './form-question/form-question.component';
import { TabsModule } from 'ngx-bootstrap/tabs';

@NgModule({
  imports: [
    QuestionsRoutingModule,
    SharedModule,
    TabsModule,
  ],
  declarations: [
    ListQuestionsComponent,
    FormQuestionComponent,
  ]
})
export class QuestionsModule { }
