import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListQuestionsComponent } from './list-questions/list-questions.component';
import { FormQuestionComponent } from './form-question/form-question.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Questions'
    },
    children: [
      {
        path: '',
        component: ListQuestionsComponent,
        data: {
          title: 'List Questions',
        },
      },
      {
        path: 'new',
        component: FormQuestionComponent,
        data: {
          title: 'New Question',
        },
      },
      {
        path: ':id',
        component: FormQuestionComponent,
        data: {
          title: 'Edit Question',
        },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class QuestionsRoutingModule { }
