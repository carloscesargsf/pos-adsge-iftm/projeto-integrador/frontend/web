import { Component, OnInit } from '@angular/core';
import { QuestionsService } from 'src/app/core/services/questions/questions.service';
import { PageInfo } from 'src/app/core/models/page-info';
import { QuestionViewItem } from 'src/app/core/models/question-view-item.model';

@Component({
  selector: 'app-list-questions',
  templateUrl: './list-questions.component.html',
  styleUrls: ['./list-questions.component.scss']
})
export class ListQuestionsComponent implements OnInit {

  payLoad: PageInfo<QuestionViewItem> = new PageInfo<QuestionViewItem>();

  constructor(private questionsService: QuestionsService) {
  }

  ngOnInit() {
    this.fetchData();
  }

  fetchData() {
    this.questionsService.findAll()
      .subscribe(payLoad => this.payLoad = payLoad);
  }

}
