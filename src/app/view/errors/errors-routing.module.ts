import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Status404Component } from './status-404/status-404.component';

const routes: Routes = [
  {
    path: '',
    component: Status404Component,
    data: {
      title: '404'
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ErrorsRoutingModule { }
