import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ErrorsModule } from './errors/errors.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  exports: [
    ErrorsModule
  ]
})
export class ViewModule { }
