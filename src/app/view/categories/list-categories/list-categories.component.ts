import { Component, OnInit } from '@angular/core';
import { CategoriesService } from 'src/app/core/services/categories/categories.service';
import { PageInfo } from 'src/app/core/models/page-info';
import { CategoryList } from 'src/app/core/models/category-list.model';

@Component({
  selector: 'app-list-categories',
  templateUrl: './list-categories.component.html',
  styleUrls: ['./list-categories.component.scss']
})
export class ListCategoriesComponent implements OnInit {

  payLoad: PageInfo<CategoryList> = new PageInfo<CategoryList>();

  constructor(private categoriesService: CategoriesService) {
  }

  ngOnInit() {
    this.fetchData();
  }

  fetchData() {
    this.categoriesService.findAll()
      .subscribe(payLoad => this.payLoad = payLoad);
  }

}
