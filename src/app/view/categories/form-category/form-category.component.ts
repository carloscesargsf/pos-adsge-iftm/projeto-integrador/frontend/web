import { Component, OnInit } from '@angular/core';
import { CategoriesService } from 'src/app/core/services/categories/categories.service';
import { CategoryInsert } from 'src/app/core/models/category-insert.model';
import { CategoryUpdate } from 'src/app/core/models/category-update.model';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup } from '@angular/forms';
import { CategoryList } from 'src/app/core/models/category-list.model';

@Component({
  selector: 'app-form-category',
  templateUrl: './form-category.component.html',
  styleUrls: ['./form-category.component.scss']
})
export class FormCategoryComponent implements OnInit {

  id: number;
  category: FormGroup;
  categories: CategoryList[] = [];

  constructor(private categoriesService: CategoriesService,
              private currentRoute: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    this.fetchData();
    this.fetchCategories();
  }

  fetchCategories() {
    this.categoriesService.findAll()
      .subscribe(payLoad => this.categories = this.category.value.id
        ? payLoad.content
        : payLoad.content.filter(pc => pc.id !== this.category.value.id));
  }

  fetchData() {
    // tslint:disable-next-line: no-string-literal
    this.currentRoute.params.subscribe(
      params => {
        // tslint:disable-next-line: no-string-literal
        this.id = params['id'];
        if (this.id) {
          this.category = CategoryUpdate.getFormGroup();
          this.categoriesService.findById(this.id).subscribe(
            category => this.category.patchValue(category),
            () => this.router.navigate(['/404']),
          );
        } else {
          this.category = CategoryInsert.getFormGroup();
        }
      },
    );
  }

  onSubmit() {
    if (this.category.valid) {
      if (!this.id) {
        this.categoriesService.create(this.category.value).subscribe(
          () => this.router.navigate(['/categories']),
          error => console.error('create error', error),
        );
      } else {
        this.categoriesService.update(this.id, this.category.value).subscribe(
          () => this.router.navigate(['/categories']),
          error => console.error('update error', error),
        );
      }
    } else {
      console.error(this.category.errors);
    }
  }

  onGoBack() {
    this.router.navigate(['/categories']);
  }

}
