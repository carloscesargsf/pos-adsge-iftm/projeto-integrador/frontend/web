import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListCategoriesComponent } from './list-categories/list-categories.component';
import { FormCategoryComponent } from './form-category/form-category.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Categories'
    },
    children: [
      {
        path: '',
        component: ListCategoriesComponent,
        data: {
          title: 'List Categories',
        },
      },
      {
        path: 'new',
        component: FormCategoryComponent,
        data: {
          title: 'New Category',
        },
      },
      {
        path: ':id',
        component: FormCategoryComponent,
        data: {
          title: 'Edit Category',
        },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CategoriesRoutingModule { }
