import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DefaultLayoutComponent } from './core/layouts/default-layout/default-layout.component';
import { Status404Component } from './view/errors/status-404/status-404.component';
import { AuthGuard } from './core/guards/auth.guard';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'categories',
    pathMatch: 'full',
  },
  {
    path: '',
    loadChildren: () => import('./view/auth/auth.module').then(m => m.AuthModule),
  },
  {
    path: '',
    component: DefaultLayoutComponent,
    data: {
      title: 'Home'
    },
    children: [
      {
        path: 'categories',
        loadChildren: () => import('./view/categories/categories.module').then(m => m.CategoriesModule),
        canActivate: [ AuthGuard ],
      },
      {
        path: 'questions',
        loadChildren: () => import('./view/questions/questions.module').then(m => m.QuestionsModule),
        canActivate: [ AuthGuard ],
      },
      {
        path: 'exams',
        loadChildren: () => import('./view/exams/exams.module').then(m => m.ExamsModule),
        canActivate: [ AuthGuard ],
      },
      {
        path: 'people',
        loadChildren: () => import('./view/people/people.module').then(m => m.PeopleModule),
        canActivate: [ AuthGuard ],
      },
      {
        path: 'error',
        loadChildren: () => import('./view/errors/errors.module').then(m => m.ErrorsModule),
      },
    ]
  },
  {
    path: '**',
    component: Status404Component
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { enableTracing: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
