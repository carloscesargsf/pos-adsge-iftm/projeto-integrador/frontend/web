import { Component, OnInit } from '@angular/core';
import { navItems } from '../../../_nav';
import { INavData } from '@coreui/angular';
import { AuthService } from '../../services/auth/auth.service';
import { Router } from '@angular/router';
import { PeopleService } from '../../services/people/people.service';
import { Token } from '../../models/token.model';
import { Observable } from 'rxjs';
import { PersonViewItem } from '../../models/person-view-item.model';

@Component({
  selector: 'app-default-layout',
  templateUrl: './default-layout.component.html',
  styleUrls: ['./default-layout.component.scss']
})
export class DefaultLayoutComponent implements OnInit {

  public sidebarMinimized: boolean;
  public navItems: INavData[];

  constructor(private peopleService: PeopleService,
              private authService: AuthService,
              private router: Router) { }

  ngOnInit() {
    this.sidebarMinimized = false;
    this.navItems = navItems;
  }

  toggleMinimize(e) {
    this.sidebarMinimized = e;
  }

  profile() {
    this.peopleService.findByEmail(this.authService.getTokenValue().email).subscribe(
      (personViewItem: PersonViewItem) => this.router.navigate(['/people', personViewItem.id]),
      () => this.logout(),
    );
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['/login']);
  }

}
