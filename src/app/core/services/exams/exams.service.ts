import { Injectable } from '@angular/core';
import { CrudService } from '../crud.service';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { ExamInsert } from '../../models/exam-insert.model';
import { ExamUpdate } from '../../models/exam-update.model';
import { ExamViewItem } from '../../models/exam-view-item.model';

@Injectable({
  providedIn: 'root'
})
export class ExamsService extends CrudService<ExamInsert, ExamUpdate, ExamViewItem, ExamViewItem, number> {

  constructor(http: HttpClient) {
    super(`${environment.apiUrl}exams`, http);
  }

}
