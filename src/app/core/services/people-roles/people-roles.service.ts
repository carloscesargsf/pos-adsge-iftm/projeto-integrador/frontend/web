import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { ReadService } from '../read.service';
import { PersonRoleViewItem } from '../../models/person-role-view-item.model';

@Injectable({
  providedIn: 'root'
})
export class PeopleRolesService extends ReadService<PersonRoleViewItem> {

  constructor(http: HttpClient) {
    super(`${environment.apiUrl}people-roles`, http);
  }

}
