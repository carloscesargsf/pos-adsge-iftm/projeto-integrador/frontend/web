import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { QuestionTypeViewItem } from '../../models/question-type-view-item.model';
import { ReadService } from '../read.service';

@Injectable({
  providedIn: 'root'
})
export class QuestionTypesService extends ReadService<QuestionTypeViewItem> {

  constructor(http: HttpClient) {
    super(`${environment.apiUrl}question-types`, http);
  }

}
