import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { CrudService } from '../crud.service';
import { CompanyInsert } from '../../models/company-insert.model';
import { CompanyViewItem } from '../../models/company-view-item.model';
import { CompanyUpdate } from '../../models/company-update.model';

@Injectable({
  providedIn: 'root'
})
export class CompaniesService extends CrudService<CompanyInsert, CompanyUpdate, CompanyViewItem, CompanyViewItem, number> {

  constructor(http: HttpClient) {
    super(`${environment.apiUrl}companies`, http);
  }

}
