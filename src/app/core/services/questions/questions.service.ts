import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { QuestionViewItem } from '../../models/question-view-item.model';
import { CrudService } from '../crud.service';
import { QuestionInsert } from '../../models/question-insert.model';
import { QuestionUpdate } from '../../models/question-update.model';

@Injectable({
  providedIn: 'root'
})
export class QuestionsService extends CrudService<QuestionInsert, QuestionUpdate, QuestionViewItem, QuestionViewItem, number> {

  constructor(http: HttpClient) {
    super(`${environment.apiUrl}questions`, http);
  }

}
