import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { ReadService } from './read.service';

export abstract class CrudService<I, U, O, L, ID> extends ReadService<L> {

  constructor(protected url: string, http: HttpClient) {
    super(url, http);
  }

  findById(id: ID): Observable<O> {
    return this.http.get<O>(`${this.url}/${id}`);
  }

  create(entity: I): Observable<O> {
    return this.http.post<O>(this.url, entity);
  }

  update(id: any, entity: U): Observable<void> {
    return this.http.put<void>(`${this.url}/${id}`, entity, {});
  }

  delete(id: ID): Observable<void> {
    return this.http.delete<void>(`${this.url}/${id}`);
  }

}
