import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient, HttpParams } from '@angular/common/http';
import { PageInfo } from '../models/page-info';

export abstract class ApiService {

  constructor(protected url: string, protected http: HttpClient) {
  }

  request(httpMethod: string, endpoint: string = '', body: any = null, params: string = null): Observable<any | any[]> {
    return this.http.request(httpMethod, this.url + endpoint, this.getRequestOptions(body, params));
  }

  protected getRequestOptions(body: any = null, params: string = null) {
    const options = {};

    if (body !== null) {
      // tslint:disable-next-line: no-string-literal
      options['body'] = body;
    }

    if (params !== null) {
      // tslint:disable-next-line: no-string-literal
      options['params'] = new HttpParams({fromString: params});
    }

    return options;
  }

}
