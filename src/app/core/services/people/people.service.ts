import { Injectable } from '@angular/core';
import { User } from '../../models/user.model';
import { CrudService } from '../crud.service';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { PersonInsert } from '../../models/person-insert.model';
import { PersonUpdate } from '../../models/person-update.model';
import { PersonViewItem } from '../../models/person-view-item.model';

@Injectable({
  providedIn: 'root'
})
export class PeopleService extends CrudService<PersonInsert, PersonUpdate, PersonViewItem, PersonViewItem, number> {

  constructor(http: HttpClient) {
    super(`${environment.apiUrl}people`, http);
  }

  findByEmail(email: string): Observable<PersonViewItem> {
    return this.request('get', '/find', null, `email=${email}`);
  }

}
