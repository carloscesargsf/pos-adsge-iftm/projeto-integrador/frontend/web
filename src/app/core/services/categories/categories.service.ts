import { Injectable } from '@angular/core';
import { CategoryInsert } from '../../models/category-insert.model';
import { CategoryUpdate } from '../../models/category-update.model';
import { CategoryViewItem } from '../../models/category-view-item.model';
import { CrudService } from '../crud.service';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { CategoryList } from '../../models/category-list.model';

@Injectable({
  providedIn: 'root'
})
export class CategoriesService extends CrudService<CategoryInsert, CategoryUpdate, CategoryViewItem, CategoryList, number> {

  constructor(http: HttpClient) {
    super(`${environment.apiUrl}categories`, http);
  }

}
