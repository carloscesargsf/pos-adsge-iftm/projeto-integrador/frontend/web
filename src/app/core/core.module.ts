import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule, registerLocaleData } from '@angular/common';
import localePtBr from '@angular/common/locales/pt';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { IConfig, NgxMaskModule } from 'ngx-mask';
import { ErrorInterceptor } from './interceptors/error-interceptor';
import { JwtInterceptor } from './interceptors/jwt-interceptor';
import { CategoriesService } from './services/categories/categories.service';
import { AuthService } from './services/auth/auth.service';
import { ExamsService } from './services/exams/exams.service';
import { QuestionsService } from './services/questions/questions.service';
import { QuestionTypesService } from './services/question-types/question-types.service';
import { PeopleService } from './services/people/people.service';
import { PeopleRolesService } from './services/people-roles/people-roles.service';

export const options: Partial<IConfig> | (() => Partial<IConfig>) = {
  decimalMarker: ',',
  thousandSeparator: '.',
  suffix: '',
  showTemplate: true,
};

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    HttpClientModule,
    NgxMaskModule.forRoot(options),
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    AuthService,
    CategoriesService,
    ExamsService,
    QuestionsService,
    QuestionTypesService,
    PeopleService,
    PeopleRolesService,
  ]
})
export class CoreModule {

  constructor( @Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule) {
      throw new Error('CoreModule has already been loaded. You should only import it in the AppModule.');
    }
    registerLocaleData(localePtBr);
  }

}
