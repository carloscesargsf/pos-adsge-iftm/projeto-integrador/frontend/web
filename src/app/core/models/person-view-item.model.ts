import { PersonRoleViewItem } from './person-role-view-item.model';

export class PersonViewItem {
  id: number;
  name: string;
  cpf: string;
  gender: string;
  email: string;
  active: boolean;
  personRoles: PersonRoleViewItem[];
}
