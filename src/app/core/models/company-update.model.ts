import { FormGroup, FormControl, Validators } from '@angular/forms';

export class CompanyUpdate {
  id: number;
  companyName: string;
  tradingName: string;
  cnpj: string;

  static getFormGroup(): FormGroup {
    return new FormGroup({
      id: new FormControl(null, [Validators.required]),
      companyName: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(200)]),
      tradingName: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(200)]),
      cnpj: new FormControl('', [Validators.required, Validators.minLength(14), Validators.maxLength(14)]),
    });
  }
}
