import { FormGroup, FormControl, Validators } from '@angular/forms';

export class CategoryInsert {
  name: string;
  description: string;
  parentCategoryId: number;

  static getFormGroup(): FormGroup {
    return new FormGroup({
      name: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(200)]),
      description: new FormControl('', []),
      parentCategoryId: new FormControl(null, []),
    });
  }
}
