export class PageInfo<E> {
  page: number;
  pageSize: number;
  totalElements: number;
  totalPages: number;
  content: E[];

  constructor(page: number = 0, pageSize: number = 0, totalElements: number = 0, totalPages: number = 0, content: E[] = []) {
    this.page = page;
    this.pageSize = pageSize;
    this.totalElements = totalElements;
    this.totalPages = totalPages;
    this.content = content;
  }
}
