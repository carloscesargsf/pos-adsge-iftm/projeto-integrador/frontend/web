import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';

export class ExamUpdate {
  id: number;
  name: string;
  description: string;
  beginDate: Date;
  endDate: Date;
  grade: number;
  questionIds: number[];
  ratedIds: number[];

  static getFormGroup(): FormGroup {
    return new FormGroup({
      id: new FormControl(null, [Validators.required]),
      name: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(200)]),
      description: new FormControl('', []),
      beginDate: new FormControl(new Date(), [Validators.required]),
      endDate: new FormControl(null, []),
      grade: new FormControl(null, []),
      questionIds: new FormArray([], []),
      ratedIds: new FormArray([], []),
    });
  }
}
