import { FormGroup, FormControl } from '@angular/forms';

export class Token {
  email: string;
  token: string;

  static getFormGroup(): FormGroup {
    return new FormGroup({
      email: new FormControl('', []),
      token: new FormControl('', []),
    });
  }
}
