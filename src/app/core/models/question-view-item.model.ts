import { PossibleAnswerViewItem } from './possible-answer-view-item.model';

export class QuestionViewItem {
  id: number;
  questionTypeId: number;
  questionTypeName: string;
  categoryId: number;
  categoryName: string;
  title: string;
  active: boolean;
  possibleAnswers: PossibleAnswerViewItem[];
}
