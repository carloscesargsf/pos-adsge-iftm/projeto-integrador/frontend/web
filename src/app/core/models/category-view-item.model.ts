export class CategoryViewItem {
  id: number;
  name: string;
  description: string;
  parentCategoryId: number;
  active: boolean;
  childCategories: CategoryViewItem[];
}
