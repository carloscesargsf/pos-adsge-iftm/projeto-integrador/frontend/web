import { FormGroup, FormControl, Validators } from '@angular/forms';

export class CompanyViewItem {
  id: number;
  companyName: string;
  tradingName: string;
  cnpj: string;
  active: boolean;
}
