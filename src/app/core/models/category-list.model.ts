export class CategoryList {
  id: number;
  name: string;
  description: string;
  active: boolean;
}
