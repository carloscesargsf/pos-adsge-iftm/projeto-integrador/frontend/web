import { ExamQuestionViewItem } from './exam-question-view-item.model';
import { ExamRatedViewItem } from './exam-rated-view-item.model';

export class ExamViewItem {
  id: number;
  evaluatorId: number;
  evaluatorName: string;
  evaluatorGender: string;
  name: string;
  description: string;
  beginDate: Date;
  endDate: Date;
  grade: number;
  active: boolean;
  questions: ExamQuestionViewItem[];
  rated: ExamRatedViewItem[];
}
