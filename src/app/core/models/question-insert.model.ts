import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { QuestionPossibleAnswer } from './question-possible-answer.model';

export class QuestionInsert {
  questionTypeId: number;
  categoryId: number;
  title: string;
  possibleAnswers: QuestionPossibleAnswer[];

  static getFormGroup(): FormGroup {
    const formGroup = new FormGroup({
      questionTypeId: new FormControl(0, [Validators.required, Validators.min(1)]),
      categoryId: new FormControl(0, [Validators.required, Validators.min(1)]),
      title: new FormControl('', [Validators.required]),
      possibleAnswers: new FormArray([], [Validators.required]),
    });

    const possibleAnswers: FormArray = formGroup.get('possibleAnswers') as FormArray;
    for (let step = 1; step <= 4; step++) {
      possibleAnswers.push(QuestionPossibleAnswer.getFormGroup());
    }

    return formGroup;
  }
}
