import { FormGroup, FormControl } from '@angular/forms';

export class Credentials {
  email: string;
  password: string;

  static getFormGroup(): FormGroup {
    return new FormGroup({
      email: new FormControl('', []),
      password: new FormControl('', []),
    });
  }
}
