import { QuestionViewItem } from './question-view-item.model';

export class ExamQuestionViewItem {
  id: number;
  examId: number;
  question: QuestionViewItem;
  active: boolean;
}
