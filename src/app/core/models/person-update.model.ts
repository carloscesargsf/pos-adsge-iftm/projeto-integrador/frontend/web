import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';

export class PersonUpdate {
  id: number;
  name: string;
  cpf: string;
  gender: string;
  email: string;
  password: string;
  roles: string[];

  static getFormGroup(): FormGroup {
    return new FormGroup({
      id: new FormControl(null, [Validators.required]),
      name: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(200)]),
      cpf: new FormControl('', [Validators.required]),
      gender: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email, Validators.minLength(1), Validators.maxLength(200)]),
      password: new FormControl('', []),
      roles: new FormArray([], []),
    });
  }
}
