export class ExamRatedViewItem {
  id: number;
  examId: number;
  ratedId: number;
  ratedName: string;
  ratedGender: string;
  active: boolean;
}
