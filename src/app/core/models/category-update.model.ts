import { FormGroup, FormControl, Validators } from '@angular/forms';

export class CategoryUpdate {
  id: number;
  name: string;
  description: string;
  parentCategoryId: number;

  static getFormGroup(): FormGroup {
    return new FormGroup({
      id: new FormControl(null, [Validators.required]),
      name: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(200)]),
      description: new FormControl('', []),
      parentCategoryId: new FormControl(null, []),
    });
  }
}
