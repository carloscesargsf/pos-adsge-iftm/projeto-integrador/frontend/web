import { FormGroup, FormControl, Validators } from '@angular/forms';

export class QuestionPossibleAnswer {
  description: string;
  correct: boolean;

  static getFormGroup(): FormGroup {
    return new FormGroup({
      description: new FormControl('', [Validators.required]),
      correct: new FormControl(false, Validators.required),
    });
  }
}
