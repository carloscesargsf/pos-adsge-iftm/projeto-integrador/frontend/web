import { FormGroup, FormControl, Validators } from '@angular/forms';
import { PersonInsert } from './person-insert.model';

export class CompanyInsert {
  companyName: string;
  tradingName: string;
  cnpj: string;
  admin: PersonInsert;

  static getFormGroup(): FormGroup {
    return new FormGroup({
      companyName: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(200)]),
      tradingName: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(200)]),
      cnpj: new FormControl('', [Validators.required, Validators.minLength(14), Validators.maxLength(14)]),
      admin: PersonInsert.getFormGroup(),
    });
  }
}
