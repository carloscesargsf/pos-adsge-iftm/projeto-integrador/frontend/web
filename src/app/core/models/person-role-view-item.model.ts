export class PersonRoleViewItem {
  id: number;
  personId: number;
  personName: string;
  personCpf: string;
  personGender: string;
  personEmail: string;
  companyId: number;
  companyName: string;
  companyTradingName: string;
  role: string;
  active: boolean;
}
