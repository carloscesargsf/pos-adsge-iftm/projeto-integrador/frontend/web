export class PossibleAnswerViewItem {
  id: number;
  questionId: number;
  questionTitle: string;
  description: string;
  correct: boolean;
  active: boolean;
}
