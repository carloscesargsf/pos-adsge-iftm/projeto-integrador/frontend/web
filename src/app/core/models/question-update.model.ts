import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { QuestionPossibleAnswer } from './question-possible-answer.model';

export class QuestionUpdate {
  id: number;
  questionTypeId: number;
  categoryId: number;
  title: string;
  possibleAnswers: QuestionPossibleAnswer[];

  static getFormGroup(): FormGroup {
    return new FormGroup({
      id: new FormControl(null, [Validators.required]),
      questionTypeId: new FormControl(0, [Validators.required, Validators.min(1)]),
      categoryId: new FormControl(0, [Validators.required, Validators.min(1)]),
      title: new FormControl('', [Validators.required]),
      possibleAnswers: new FormArray([], [Validators.required]),
    });
  }
}
