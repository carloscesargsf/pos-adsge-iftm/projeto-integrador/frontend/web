import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';

export class PersonInsert {
  name: string;
  cpf: string;
  gender: string;
  email: string;
  password: string;
  roles: string[];

  static getFormGroup(): FormGroup {
    return new FormGroup({
      name: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(200)]),
      cpf: new FormControl('', [Validators.required, Validators.minLength(11), Validators.maxLength(11)]),
      gender: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.email, Validators.minLength(1), Validators.maxLength(200)]),
      password: new FormControl('', [Validators.required, Validators.minLength(8), Validators.maxLength(200)]),
      roles: new FormArray([], []),
    });
  }
}
