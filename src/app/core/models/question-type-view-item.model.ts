export class QuestionTypeViewItem {
  id: number;
  name: string;
  description: string;
  active: boolean;
}
